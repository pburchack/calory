from django.urls import path
from . import views
from .views import ProductOfNameListView, ProductOfCaloriesListView, ProductDetailView, add_to_dict, view_dict, remove_from_dict, clear_dict, add_product

urlpatterns = [
    path('', views.index, name='index'),
    path('counter', views.counter, name='counter'),
    path('all-of-name/', ProductOfNameListView.as_view(), name='product_of_name'),
    path('all-of-calories/', ProductOfCaloriesListView.as_view(),
         name='product_of_calories'),
    path('product/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('add_to_dict/<int:pk>/', add_to_dict, name='add_to_dict'),
    path('view_dict/', view_dict, name='view_dict'),
    path('remove-from-dict/<int:product_id>/',
         views.remove_from_dict, name='remove_from_dict'),
    path('clear_dict/', views.clear_dict, name='clear_dict'),
    path('add_product/', views.add_product, name='add_product'),
]
