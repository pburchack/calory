from django.db import models


class Products(models.Model):
    name = models.CharField(max_length=100)
    calories = models.IntegerField()
    proteins = models.DecimalField(max_digits=10, decimal_places=2)
    fats = models.DecimalField(max_digits=10, decimal_places=2)
    carbohydrates = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.name
