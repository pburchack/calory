from decimal import Decimal
from typing import Any
from django.db.models import Q
from django.db.models.query import QuerySet
from .models import Products
from .forms import ProductForm
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages


def add_product(request):
    if request.method == "POST":
        form = ProductForm(request.POST)
        if form.is_valid():
            product_name = form.cleaned_data['name']
            if Products.objects.filter(name=product_name).exists():
                messages.error(request, 'Продукт з такою назвою вже існує.')
            else:
                form.save()
                messages.success(request, 'Продукт успішно додано.')
                return redirect('add_product')  # Змінити на відповідний URL
    else:
        form = ProductForm()
    return render(request, 'calculator/add_product.html', {'form': form})


def add_to_dict(request, pk):
    product = get_object_or_404(Products, pk=pk)
    if request.method == 'POST':
        weight = Decimal(request.POST.get('weight', '100'))

        if 'product_dict' not in request.session:
            request.session['product_dict'] = {}

        product_dict = request.session['product_dict']
        product_dict[pk] = {
            'name': product.name,
            'weight': float(weight),
            'calories': float(Decimal(product.calories) * weight / Decimal('100')),
            'proteins': float(Decimal(product.proteins) * weight / Decimal('100')),
            'fats': float(Decimal(product.fats) * weight / Decimal('100')),
            'carbohydrates': float(Decimal(product.carbohydrates) * weight / Decimal('100')),
        }
        request.session['product_dict'] = product_dict

        messages.success(request, 'Продукт успішно додано до словника.')
        return redirect('product_detail', pk=pk)
    else:
        return render(request, 'calculator/product_detail.html', {'product': product})


def remove_from_dict(request, product_id):
    product_dict = request.session.get('product_dict', {})

    # Видалення продукту зі словника за його ID
    if str(product_id) in product_dict:
        del product_dict[str(product_id)]

    request.session['product_dict'] = product_dict
    return redirect('view_dict')


def clear_dict(request):
    request.session['product_dict'] = {}
    return redirect('view_dict')


def view_dict(request):
    product_dict = request.session.get('product_dict', {})

    # Ініціалізуємо загальні значення
    total_weight = 0
    total_calories = 0
    total_proteins = 0
    total_fats = 0
    total_carbohydrates = 0

    # Підрахунок загальних значень
    for product in product_dict.values():
        if product['weight'] == None:
            product['weight'] == 0
            total_weight += float(product['weight'])
        total_weight += float(product['weight'])
        total_calories += float(product['calories'])
        total_proteins += float(product['proteins'])
        total_fats += float(product['fats'])
        total_carbohydrates += float(product['carbohydrates'])

    context = {
        'product_dict': product_dict,
        'total_weight': total_weight,
        'total_calories': total_calories,
        'total_proteins': total_proteins,
        'total_fats': total_fats,
        'total_carbohydrates': total_carbohydrates,
    }
    return render(request, 'calculator/view_dict.html', context)


def index(request):
    return render(request, 'calculator/index.html')


def counter(request):
    return render(request, 'calculator/counter.html')


class ProductListView(ListView):
    # Клас для перегляду всіх об'єктів з моделі
    model = Products
    template_name = 'calculator/page_of.html'
    paginate_by = 10  # Number of products per page
    ordering = ['name']

    def get_queryset(self) -> QuerySet[Any]:
        # Метод для вибірки об'єктів з моделі
        queryset = super().get_queryset()
        search_query = self.request.GET.get('search', '').strip()
        if search_query:
            queryset = queryset.filter(
                Q(name__icontains=search_query) |
                Q(name__iregex=r'\b{}\b'.format(search_query))
            )
        return queryset

    def get_context_data(self, **kwargs) -> dict:
        # Метод для підготовки контексту для шаблону
        context = super().get_context_data(**kwargs)
        search_query = self.request.GET.get('search', '').strip()
        context['search_query'] = search_query
        if search_query:
            # Counting the results for search query
            context['search_results_count'] = self.get_queryset().count()
        return context


class ProductDetailView(DetailView):
    # клас для детального представлення об'єкту моделі
    model = Products
    template_name = 'calculator/product_detail.html'
    context_object_name = 'product'


class ProductOfNameListView(ProductListView):
    ordering = ['name']


class ProductOfCaloriesListView(ProductListView):
    ordering = ['calories']
