import json
from django.core.exceptions import ObjectDoesNotExist
from calculator.models import Products  # Замініть на вашу модель

# Зчитуємо дані з JSON-файлу
with open('data.json', 'r', encoding='utf-8') as f:
    data = json.load(f)

# Імпортуємо дані в модель Django
for item in data:
    try:
        obj = Products.objects.get(id=item['id'])  # Замініть на ваше поле id
    except ObjectDoesNotExist:
        obj = Products()

    obj.name = item['name']  # Замініть на ваші поля
    obj.calories = item['calories']
    obj.proteins = item['proteins']
    obj.fats = item['fat']
    obj.carbohydrates = item['carbohydrates']
    obj.save()
