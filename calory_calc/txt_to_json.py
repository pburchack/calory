import json

# Зчитуємо дані з файлу
with open('products.txt', 'r', encoding='utf-8') as f:
    lines = f.readlines()

# Обробляємо кожен рядок
data = []
for i, line in enumerate(lines):
    # Розділяємо рядок на частини
    parts = line.strip().split('|')
    # Видаляємо пробіли з країв кожної частини
    parts = [part.strip() for part in parts]
    # Додаємо дані в список
    data.append({
        "id": i+1,
        "name": parts[1],
        "calories": int(parts[2]),
        "proteins": float(parts[3].replace(',', '.')),
        "fat": float(parts[4].replace(',', '.')),
        "carbohydrates": float(parts[5].replace(',', '.'))
    })

# Зберігаємо дані в JSON-файл
with open('output.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, ensure_ascii=False, indent=4)
